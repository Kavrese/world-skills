package com.example.wsr0.AuthScreen

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.example.wsr0.MainScreen.MainActivity
import com.example.wsr0.R
import com.example.wsr0.RegScreen.RegActivity
import com.example.wsr0.common.*
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        auth.setOnClickListener {
            val email = email_auth.text.toString()
            val password = password_auth.text.toString()
            if (email.isNotEmpty() && password.isNotEmpty()){
                if (validateEmail(email))
                    auth(this, email, password, object: OnComplete {
                        override fun onGood(modelAnswer: ModelAnswer) {
                            startActivity(Intent(this@AuthActivity, MainActivity::class.java))
                            finish()
                        }
                    })
                else{
                    showAlertDialog(this, "Ошибка", "Почта не корректна")
                }
            }else{
                showAlertDialog(this, "Ошибка", "Заполните все поля")
            }
        }

        to_reg_2.setOnClickListener {
            startActivity(Intent(this, RegActivity::class.java))
        }
    }
}