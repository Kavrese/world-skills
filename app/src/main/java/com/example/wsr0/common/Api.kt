package com.example.wsr0.common

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {
    @POST("user/login")
    fun auth(@Body body: BodyAuth): Call<ModelAnswer>

    @GET("feelings")
    fun feelings(): Call<ModelAnswerFeeling>

    @GET("quotes")
    fun quotes(): Call<ModelAnswerQuotes>
}