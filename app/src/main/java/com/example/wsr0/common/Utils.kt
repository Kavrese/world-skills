package com.example.wsr0.common

import android.app.AlertDialog
import android.content.Context
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun initRetrofit(): Api {
    return Retrofit.Builder()
        .baseUrl("http://mskko2021.mad.hakta.pro/api/")
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(Api::class.java)
}

fun showAlertDialog(context: Context, title: String, message: String){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage(message)
        .setNegativeButton("OK", null)
        .show()
}

fun showAlertDialog(context: Context, title: String, t: Throwable){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage(t.message)
        .setNegativeButton("OK", null)
        .show()
}

fun showAlertDialog(context: Context, title: String){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage("Повторите попытку позже !")
        .setNegativeButton("OK", null)
        .show()
}

fun validateEmail(email: String): Boolean{
    return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
}

fun auth(context: Context, username: String, password: String, onComplete: OnComplete){
    initRetrofit().auth(BodyAuth(username, password)).enqueue(object: Callback<ModelAnswer>{

        override fun onResponse(call: Call<ModelAnswer>, response: Response<ModelAnswer>) {
            if (response.body() != null){
                onComplete.onGood(response.body()!!)
                Info.token = response.body()!!.token
            }else{
                showAlertDialog(context, "Ошибка авторизации")
            }
        }

        override fun onFailure(call: Call<ModelAnswer>, t: Throwable) {
            showAlertDialog(context, "Ошибка авторизации", t)
        }
    })
}