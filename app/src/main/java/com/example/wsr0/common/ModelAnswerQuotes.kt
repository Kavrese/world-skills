package com.example.wsr0.common

data class ModelAnswerQuotes (
    val success: Boolean,
    val data: List<ModelQuotes>
        )

data class ModelQuotes (
    val id: Int,
    val title: String,
    val image: String,
    val description: String
)
