package com.example.wsr0.common

data class BodyAuth(
    val email: String,
    val password: String
)
