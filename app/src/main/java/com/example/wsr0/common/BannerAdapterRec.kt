package com.example.wsr0.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.wsr0.R
import kotlinx.android.synthetic.main.item_block.view.*

class BannerAdapterRec(var list: MutableList<ModelFeeling>): RecyclerView.Adapter<BannerAdapterRec.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_block, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.block_title.text = list[position].title
        Glide.with(holder.itemView.context)
            .load(list[position].image)
            .into(holder.itemView.img_block)
    }

    override fun getItemCount(): Int = list.size

    fun updateData(list: MutableList<ModelFeeling>){
        this.list = list
        notifyDataSetChanged()
    }
}