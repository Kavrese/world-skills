package com.example.wsr0.common

data class ModelAnswer(
    val id: String,
    val email: String,
    val nickName: String,
    val avatar: String,
    val token: String
)
