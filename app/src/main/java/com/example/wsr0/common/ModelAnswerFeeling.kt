package com.example.wsr0.common

data class ModelAnswerFeeling(
    val success: Boolean,
    val data: List<ModelFeeling>
)

data class ModelFeeling (
    val id: Int,
    val title: String,
    val position: Int,
    val image: String
)
