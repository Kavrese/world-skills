package com.example.wsr0.OnBoardingScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.wsr0.AuthScreen.AuthActivity
import com.example.wsr0.R
import com.example.wsr0.RegScreen.RegActivity
import kotlinx.android.synthetic.main.activity_on_boarding.*

class OnBoardingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)

        to_auth.setOnClickListener{
            startActivity(Intent(this@OnBoardingActivity, AuthActivity::class.java))
            finish()
        }

        to_reg.setOnClickListener{
            startActivity(Intent(this@OnBoardingActivity, RegActivity::class.java))
        }
    }
}