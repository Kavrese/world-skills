package com.example.wsr0.BlockScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.example.wsr0.OnBoardingScreen.OnBoardingActivity
import com.example.wsr0.R
import com.example.wsr0.common.BannerAdapterRec
import com.example.wsr0.common.ModelAnswerFeeling
import com.example.wsr0.common.initRetrofit
import com.example.wsr0.common.showAlertDialog
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_block.*
import kotlinx.android.synthetic.main.custom_tab.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BlockActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_block)

        viewPager2.adapter = BannerAdapterRec(arrayListOf())
        tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab!!.customView!!.dot_img.setImageResource(R.drawable.dot_select)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab!!.customView!!.dot_img.setImageResource(R.drawable.dot)
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })

        viewPager2.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                if (position == 0){
                    left_text.text = "Пропустить"
                    left_text.setOnClickListener {
                        startActivity(Intent(this@BlockActivity, OnBoardingActivity::class.java))
                        finish()
                    }
                }else
                    if (position == (viewPager2.adapter)!!.itemCount - 1){
                        right_text.text = "Аутентификация"
                        right_text.setOnClickListener {
                            startActivity(Intent(this@BlockActivity, OnBoardingActivity::class.java))
                            finish()
                        }
                    }else{
                        left_text.text = "Назад"
                        left_text.setOnClickListener {
                            viewPager2.setCurrentItem(position - 1, true)
                        }
                        right_text.text = "Далее"
                        right_text.setOnClickListener {
                            viewPager2.setCurrentItem(position + 1, true)
                        }
                    }
            }
        })

        initRetrofit().feelings().enqueue(object: Callback<ModelAnswerFeeling>{
            override fun onResponse(
                call: Call<ModelAnswerFeeling>,
                response: Response<ModelAnswerFeeling>
            ) {
                if (response.body() != null){
                    (viewPager2.adapter as BannerAdapterRec).updateData(response.body()!!.data.toMutableList())
                    TabLayoutMediator(tabLayout, viewPager2){tab, position ->
                        tab.customView = LayoutInflater.from(this@BlockActivity).inflate(R.layout.custom_tab, null)
                    }.attach()
                }else{
                    showAlertDialog(this@BlockActivity, "Ошибка получения")
                }
            }

            override fun onFailure(call: Call<ModelAnswerFeeling>, t: Throwable) {
                showAlertDialog(this@BlockActivity, "Ошибка получения", t)
            }

        })
    }
}