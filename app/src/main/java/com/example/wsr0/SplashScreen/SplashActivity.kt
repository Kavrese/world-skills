package com.example.wsr0.SplashScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.wsr0.AuthScreen.AuthActivity
import com.example.wsr0.BlockScreen.BlockActivity
import com.example.wsr0.R

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            val sh = getSharedPreferences("0", 0)
            if (sh.getBoolean("isFirst", true)){
                startActivity(Intent(this@SplashActivity, BlockActivity::class.java))
            }else{
                startActivity(Intent(this@SplashActivity, BlockActivity::class.java))
            }
             finish()
        }, 1500)
    }
}