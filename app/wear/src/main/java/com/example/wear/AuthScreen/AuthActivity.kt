package com.example.wear.AuthScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.wear.R
import com.example.wear.MainScreen.MainActivity
import com.example.wsr0.common.*
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        auth.setOnClickListener {
            val username = username_auth.text.toString()
            val password = password_auth.text.toString()
            if (username.isNotEmpty() && password.isNotEmpty()){
                auth(this, username, password, object: OnComplete {
                    override fun onGood(modelAnswer: ModelAnswer) {
                        startActivity(Intent(this@AuthActivity, MainActivity::class.java))
                        finish()
                    }
                })
            }else{
                showAlertDialog(this, "Ошибка", "Заполните все поля")
            }
        }
    }
}